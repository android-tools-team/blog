---
title: Android Tools is a project in Debian Summer of Code again
author: Hans-Christoph Steiner
---

Debian has participated in Google Summer of Code (GSoC) for many years now, and after a one year hiatus, Debian is applying again this year.  The Android Tools Team will participate again this year.  Towards that end, I just posted the project description for the application:

<https://wiki.debian.org/SummerOfCode2024/PendingProjects/AndroidSDKToolsInDebian>

First, Debian needs to be approved by Google to be part of the project.  Then we will open it up to applications from students around the world to join us in working on packaging Android SDK and Tools in Debian.  GSoC students, with help from the Debian mentors, have done key pieces of work in the Android Tools Team.  We look forward to the contributions and improvements from this round.
