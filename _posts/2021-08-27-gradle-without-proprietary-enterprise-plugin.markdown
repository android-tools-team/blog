---
title: Gradle without proprietary enterprise plugin
author: Phil Morrell
---

We have Gradle v4.4 in Debian, the current packaging effort is focussed on v6.4,
and upstream is now up to v7.2. The build depends on `com.gradle.enterprise`,
which used to be an optional module called `com.gradle.build-scan` in
v5.x. [Upstream asserts](https://github.com/gradle/gradle/issues/16439) that
it's possible to build without it, but our [initial
attempt](https://salsa.debian.org/android-tools-team/admin/-/issues/33) was
unsuccessful.

Most of the problem comes from massive differences in tooling
expectations. Their development setup involves a GUI project tool and an online
build, while Debian requires offline builds and chroot management. Therefore
while ideally we'd like a patch to the Debian packaging, the bulk of the work
required would be satisfied by a patch to be used in the upstream setup.

It is assumed that if co-ordination with upstream is required, that they will
only respond to work based upon their latest release. If that approach is taken,
then the necessary changes will also need backporting to v6.4.1 so that we can
finish off the Debian packaging.

The good news is that this project has been
[approved](https://salsa.debian.org/freexian-team/project-funding/-/blob/c2c3045ac4af88b3967704b24ddf0399bd124750/accepted/2021-08-gradle-enterprise.md)
for funding as part of [Freexian Project
Funding](https://salsa.debian.org/freexian-team/project-funding/-/blob/master/README.md)
