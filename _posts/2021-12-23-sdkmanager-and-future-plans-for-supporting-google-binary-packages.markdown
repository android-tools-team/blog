---
title: sdkmanager and future plans for supporting Google binary packages
author: Hans-Christoph Steiner
---

The F-Droid [reimplementation](https://gitlab.com/fdroid/sdkmanager) of [_sdkmanager_](https://salsa.debian.org/python-team/packages/sdkmanager) is now in Debian!  This means that users now have an easy, documented way to get Google's Android SDK packages.  It also gives us a place to build a channel to help users move to free packages as they are available (e.g. https://gitlab.com/android-rebuilds).

The [_google-android-*_ packages](https://salsa.debian.org/android-tools-team/google-android-installers) were an early way to get some pieces of the Android SDK into Debian.  They are contrib because they download and install Google's binaries.  My feeling is that we as a team don't want to expand this approach, and want to use it only for limited cases where it provides value over sdkmanager.

One example of a use case is for non-technical people to use the Android emulator.  It should be possible to use the Android emulator only by installing a package.  Things like the versioned "platforms", NDK, old build-tools releases, and emulator system images are just much better managed from _sdkmanager_.  It might make sense to have one NDK in Debian built from source.
